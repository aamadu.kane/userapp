package com.amadou.userapp;

import com.amadou.userapp.dao.UserRepository;
import com.amadou.userapp.entity.Gender;
import com.amadou.userapp.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class UserAppApplicationTests {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private UserRepository userRepository;
//Integration test
	@Test
	void registrationWorksThroughAllLayers() throws Exception {
		User user = new User();
		user.setUserName("JeanKANE");
		user.setPhoneNumber("+221770000000");
		user.setGender(Gender.MALE);
		user.setCountryOfResidence("FRENCH");
		user.setBirth(LocalDate.of(2000,01,02));

		mockMvc.perform(post("/user/new-user")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk());

		User userEntity = userRepository.findByUserName("JeanKANE");
		assertThat(userEntity.getPhoneNumber()).isEqualTo("+221770000000");
	}
	// Unit test
	@Test
	void whenUserIdIsProvided_thenRetrievedNameIsCorrect() {
		String userName = userRepository.findById(1000L).get().getUserName();
		Assert.assertEquals("Amadou", userName);
	}

}
