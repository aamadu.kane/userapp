package com.amadou.userapp.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;
import static com.google.common.base.Predicates.or;

@Configuration
public class SwaggerConfig {
    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("public-api")
                .apiInfo(apiInfo()).select().paths(userPaths()).build();
    }

    private Predicate<String> userPaths() {
      //  return or(regex("/users.*"), regex("/user.*"));
        return or(t -> t.contains("/users.*"), t -> t.contains("/user.*"));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("USER APP API")
                .description("USer registration application")
                .termsOfServiceUrl("http://atos.net")
               .contact(new Contact("Amadou", "atos.net", "\"test@gmail.com\"")).license("Free License")
                .licenseUrl("kaneamadou853@gmail.com").version("1.0").build();
    }
}
