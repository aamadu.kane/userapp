package com.amadou.userapp.service;

import com.amadou.userapp.entity.User;

import java.util.List;

public interface UserService {
    public User findUser(Long id);
    public void saveUser(User user);
    public List<User> findAllUsers();
}
