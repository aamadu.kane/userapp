package com.amadou.userapp.service;

import com.amadou.userapp.dao.UserRepository;
import com.amadou.userapp.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;
    @Override
    public User findUser(Long id) {
        return this.userRepository.findById(id).get();
    }

    @Override
    public void saveUser(User user) {
        this.userRepository.saveAndFlush(user);
    }

    @Override
    public List<User> findAllUsers() {
        return this.userRepository.findAll();
    }
}
