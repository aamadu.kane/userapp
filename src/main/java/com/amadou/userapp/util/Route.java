package com.amadou.userapp.util;

public class Route {
    public static final String ALL_USERS = "/users";
    public static final String CREATE_USER = "/user/new-user";
    public static final String GET_USER_INFO = "/user-info";
}
