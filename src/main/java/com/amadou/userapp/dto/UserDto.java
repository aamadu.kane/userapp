package com.amadou.userapp.dto;

import com.amadou.userapp.entity.Gender;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;
@Data
public class UserDto {
    private Long id;
    @Size(min = 2, message = "user name should have at least 2 characters")
    @NotEmpty
    private String userName;
    @NotEmpty(message = "Country name must be filled")
    private String countryOfResidence;
    private String phoneNumber;

    private Gender gender;
    @Past(message = "Not possible for minor")
    @NotNull(message = "Mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private LocalDate birth;
}
