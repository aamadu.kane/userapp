package com.amadou.userapp.controller;

import com.amadou.userapp.annotation.LogExecutionTime;
import com.amadou.userapp.dto.UserDto;
import com.amadou.userapp.entity.User;
import com.amadou.userapp.service.UserService;
import com.amadou.userapp.util.Route;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.el.parser.ParseException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
@Api(value = "User resource endpoints", description = "All endpoints allow to manipulate the user resource")
@RestController
@CrossOrigin(origins = "*")
public class UserController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private UserService userService;
    private ModelMapper modelMapper;
    @Autowired
    public UserController(UserService userService, ModelMapper modelMapper){
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    /**
     * Create new user
     * @param userDto
     * @return
     */
    @ApiOperation(value = "User creation")
    @LogExecutionTime
    @PostMapping(Route.CREATE_USER)
    public ResponseEntity createUser(@Valid @RequestBody UserDto userDto) {
        //Check if French citizen
        if (!userDto.getCountryOfResidence().equals("FRENCH")){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Only french citizen are allowed");
        }
        // Check if user is not minor
        if(Period.between(userDto.getBirth(), LocalDate.now()).getYears() < 18){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Minors are not allowed");
        }
        //try to save the new user
        try {
            this.userService.saveUser(convertToEntity(userDto));
            return ResponseEntity.ok("User created, thanks");
        } catch (ParseException e) {
            throw new RuntimeException(e);

        }

    }

    /**
     * Return information about a user
     * @param id
     * @return userDto
     */
    @ApiOperation(value = "Get a specific user informations", response = UserDto.class)
    @LogExecutionTime
    @GetMapping(Route.GET_USER_INFO)
    public UserDto getUserInfo(@RequestParam Long id){
        return convertToDto(this.userService.findUser(id));
    }

    /**
     * Return all user
     * @return users
     */
    @ApiOperation(value = "Get all users", response = UserDto.class)

    @LogExecutionTime
    @GetMapping(Route.ALL_USERS)
    public List<UserDto> findAllUsers(){
        return this.userService.findAllUsers().stream().map((user -> convertToDto(user))).collect(Collectors.toList());
    }

    // Convert user to dto
    private UserDto convertToDto(User user) {
        UserDto userDto = modelMapper.map(user, UserDto.class);
        return userDto;
    }


    // Convert dto to user
    private User convertToEntity(UserDto userDto) throws ParseException {
        User user = modelMapper.map(userDto, User.class);
        return user;
    }
}
