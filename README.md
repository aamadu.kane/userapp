# USER MANAGEMENT APP

## Introduction
User manegement application

## Running the application
To run the application make sure you have this environment
### backend
* Java 8
* maven 3
* git
* gitlab account (for cloning)
### frontend request
postman(collection to import is joined and have all requests)

To run this application flow this these steps.
### Running backend service
Installing backend project: "mvn install".   
Make sure first 8080 port is available.  
Run the application.
If running, you can access your in memory h2  database via http://localhost:8080/h2-console.
With username: sa and without password(see application.properties to change).
Some customers are already put(from data.sql) and visible from frontend.

### Running frontend
Open postman and import the collection
Then you can run all requests
* all users request
* one user request
* user creation request
* error simulation requests
#### Test
You can run test by: mvn test
